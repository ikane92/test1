#include <stdio.h>

int
main(void)
{
    // get user to input days in month  >=28 and <=31
    int n;
    do 
    {
        printf("Days in month:  ");
        scanf("%d",&n);
    }
    while (n < 28 || n > 31);

    // get user to input pennies >=1
    unsigned int p;
    do
    {    
        printf("Pennies on first day:  ");
        scanf("%d",&p);
    } 
    while (p < 1);   
    
    // double pennies for n days
    int i;
    for (i = 1; i <= n; i++)
    {
        p *= 2;
    }
    printf("$%.2f\n", (float) p/100);
}
